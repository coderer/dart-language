/**
 * 闭包
 * @date & @author: 2018/12/17 17:04 & lxl
 */
void main() {
  var func = a();
  func();
  func();
  func();
  func();
}

a() {
  int count = 0;

//  printCount() {
//    print(count++);
//  }
//
//  return printCount;

  return () {
    print(count++);
  };
}
