/**
 * 匿名方法
 * @date & @author: 2018/12/17 16:55 & lxl
 */
void main() {
  var func = (name, age) {
    print('name = $name, age = $age');
  };

  func('rose', 18);

  var list2 = ['1', '2', '3', '4', '5'];

  print(listTimes(list2, (str) {
    return str * 3;
  }));
}

List listTimes(List list, String copy(str)) {
  for (var i = 0; i < list.length; ++i) {
    list[i] = copy(list[i]);
  }
  return list;
}
