/**
 * 方法对象
 * @date & @author: 2018/12/17 16:22 & lxl
 */
void main() {
  // 将方法赋值给一个对象, 注意后面没有括号
  Function func = printHello;
  func();

  // 将方法作为参数传递
  var list = [1, 2, 3, 4, 5];
  list.forEach(print);

  var list2 = ['1', '2', '3', '4', '5'];

  print(listTimes(list2, copy));
}

void printHello() {
  print('hello');
}

List listTimes(List list, String copy(str)) {
  for (var i = 0; i < list.length; ++i) {
    list[i] = copy(list[i]);
  }
  return list;
}

String copy(str) {
  return str * 3;
}
