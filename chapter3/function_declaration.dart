/**
 * 方法
 * @date & @author: 2018/12/17 16:22 & lxl
 */
void main() {
  print(getPerson("jack", 22));
  print(getPersonArrow("jack", 22));
  print(getPersonChoiceNameSpace("jack", age: 22));
  print(getPersonChoiceLocation("jack", 22, "男"));
  print(getPersonChoiceLocationDefault("jack"));
}

/**
 * 普通方法
 */
String getPerson(String name, int age) {
  return "name = $name, age = $age";
}

/**
 * 箭头方法
 */
String getPersonArrow(String name, int age) => "name = $name, age = $age";

/**
 * 可选命名参数
 */
String getPersonChoiceNameSpace(String name, {int age, String gender}) {
  return "name = $name, age = $age, gender = $gender";
}

/**
 * 可选位置参数
 */
String getPersonChoiceLocation(String name, [int age, String gender]) {
  return "name = $name, age = $age, gender = $gender";
}

/**
 * 可选位置参数, 指定默认值
 */
String getPersonChoiceLocationDefault(String name,
    [int age = 18, String gender = '男']) {
  return "name = $name, age = $age, gender = $gender";
}
