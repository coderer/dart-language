/**
 * 内置类型 - boolean
 * @date & @author: 2018/12/17 15:14 & lxl
 */
void main() {
  bool a = true;
  bool b = false;

  print(a);
  print(b);
}
