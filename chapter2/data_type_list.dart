/**
 * 内置类型 - list
 * list中可以放任意类型的元素
 * @date & @author: 2018/12/17 14:20 & lxl
 */
void main() {
  // 直接创建
  var list1 = [1, 2, 3];

  // 创建不可变list
  var list2 = const [4, 5, 6];
  // 不能往不可变list中添加或者从中移除元素
//  list2.add(7);
//  list2.removeLast();

  // 对象创建
  var list3 = new List();
  list3.add(1);
  list3.add(2.5);
  list3.add('3');
  list3.add(true);

  print(list1);
  print(list1[1]);

  // 修改list中的值
  list1[2] = 88;
  print(list1);

  // 移除元素
  list1.removeAt(0);
  list1.remove(88);
  print(list1);

  print(list2);
  print(list3);

  // 遍历list
  list2.forEach(print);
}
