/**
 * dart 的变量和常量
 * @date & @author: 2018/12/17 14:08 & lxl
 */
void main() {
  // 声明一个变量, 默认值为null
  var a;
  print(a);

  // 声明一个变量, 初始值为123
  var b = 456;
  print(b);

  // 改变一个变量值
  b = 222;
  print(b);

  // 使用final修改一个变量, 这个变量的值无法再次修改
  final c = 345;
  print(c);
  // 再次修改会报错
  // c = 889;

  // 声明并初始化一个常量
  const d = 456;
  print(d);
}
