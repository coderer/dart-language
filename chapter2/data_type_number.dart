/**
 * 内置类型 - number
 * number包含int和double
 * @date & @author: 2018/12/17 14:20 & lxl
 */
void main() {
  num a = 10;
  print(a);
  a = 12.5;
  print(a);

  int b = 10;
  print(b);
//  报错, int型变量不能赋double值
//  b = 80.3;

  double c = 20.5;
  print(c);
  // 不报错, 虽然赋的是int值, 但是会显示为double值 39.0
  c = 39;
  print(c);

  int one = 10, another = 3;
  double third = 20.9;

  print(one + another);
  print(one - another);
  print(one * another);
  print(one / another);
  print(one % another);

  // 整除(两个数相除后取整)
  print(one ~/ another);

  // NaN
  print(0.0 / 0.0);

  // 是否是数字
  print(third.isNaN);

  // 是否为偶数
  print(one.isEven);

  // 是否为奇数
  print(one.isOdd);

  // 求绝对值
  print(third.abs());

  // 返回最接近的整数(20.5 = 21, 20.4 = 20)
  print(third.round());

  // 向下取整
  print(third.floor());

  // 向上取整
  print(third.ceil());

  // 转换为int, 会丢掉小数点后的数据(不会四舍五入)
  print(third.toInt());

  // 转换为double
  print(one.toDouble());
}
