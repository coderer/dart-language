/**
 * 内置类型 - dynamic(动态类型, 类型var)
 * @date & @author: 2018/12/17 15:52 & lxl
 */
void main() {
  dynamic a = 20;
  a = 'hello';

  print(a);

  var list = new List<dynamic>();
  list.add(2);
  list.add("world");
  list.add(true);

  print(list);
}
