/**
 * 内置类型 - string
 *
 * @date & @author: 2018/12/17 14:50 & lxl
 */
void main() {
  const v = 100;

  // 单引号(可以使用插值表达式)
  String str1 = 'abc ${v + 1}';

  // 双引号(可以使用插值表达式)
  String str2 = "abcd $v";

  // 三引号(可以保持多行的格式)(可以使用插值表达式)
  String str3 = '''a
                   b
                   c ${v}''';

  // 原始字符串(里面的\n换行不会起做用, ${v}变量也不会输入, 会原样输出)
  String str4 = r'hello \n world${v} ';

  print(str1);
  print(str2);
  print(str3);
  print(str4);

  print(str1 + str2);
  print(str1 * 3);

  // 会比较内容和地址
  print(str1 == str2);

  // 根据索引取值
  print(str1[2]);

  print(str1.length);
  print(str1.isEmpty);
  print(str1.isNotEmpty);
  print(str1.contains("a"));
//  print(str1.substring(1));
  print(str1.startsWith("a"));
  print(str1.endsWith("1"));
}
