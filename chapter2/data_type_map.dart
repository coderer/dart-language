/**
 * 内置类型 - list
 * map中键值都可以放'任意'类型的元素(不可变map时，键放double时会报错,但能运行)
 * @date & @author: 2018/12/17 15:36 & lxl
 */
void main() {
  // 直接创建
  var map1 = {'a': 'b', 2: true, 34.3: 'e'};

  // 创建不可变map
  var map2 = const {'a': 'b', 2: true, 343: 'e'};
  // 不能修改, 会报错
//  map2['a'] = 'er';

  // 对象创建
  var map3 = new Map();

  print(map1);

  print(map1['a'] == 'b');
  // 修改
  map1['a'] = 'fdfd';
  print(map1);

  print(map2);
  print(map3);

  // 遍历, 需要传入一个方法
  map2.forEach(f);
}

void f(key, value) {
  print("key = $key, value = $value");
}
